package skint16_test

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"swissknife/types/int16"
)

var _ = Describe("Int16", func() {
	var ctl *gomock.Controller

	BeforeSuite(func() {
		ctl = gomock.NewController(GinkgoT())
	})

	AfterSuite(func() {
		ctl.Finish()
	})

	It("should initialize obj", func() {
		o := skint16.New()
		Expect(o).Should(Not(BeNil()))
	})

	It("should initialize from int", func() {
		o := skint16.From(10)
		Expect(o).Should(Not(BeNil()))
	})

	It("should compare equals", func() {
		o := skint16.From(1)
		o2 := skint16.From(1)

		Expect(o.Eq(o2)).Should(BeTrue())
	})

	It("should compare less than", func() {
		o := skint16.From(1)
		o2 := skint16.From(2)

		Expect(o.LessThan(o2)).Should(BeTrue())
	})

	It("should compare grater than", func() {
		o := skint16.From(2)
		o2 := skint16.From(1)

		Expect(o.GreaterThan(o2)).Should(BeTrue())
	})

	It("should represents to string", func() {
		o := skint16.From(10)
		str := "10"
		Expect(o.ToStr()).Should(Equal(str))
	})
})
