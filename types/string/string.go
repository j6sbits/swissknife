package skstring

import "swissknife/contracts"

type String string

func New() String{
	return ""
}

func From(value String) String {
	ins := New()
	ins = value
	return ins
}

func (i String) ToStr() string {
	return string(i)
}

func (i String) Eq(another contracts.Comparable) bool {
	return i == another.(String)
}

func (i String) LessThan(another contracts.Comparable) bool {
	return i < another.(String)
}

func (i String) GreaterThan(another contracts.Comparable) bool {
	return i > another.(String)
}
