package set

import "swissknife/contracts"

type Set struct {
	Size int
	items map[string]contracts.T
}

func New() *Set {
	return &Set{
		items: make(map[string]contracts.T),
		Size: 0,
	}
}

func From(items []contracts.T) *Set {
	s := New()
	for _, it := range items {
		s.Add(it)
	}

	return s
}

func (s *Set) Add(item contracts.T) {
	if _, exists := s.items[item.ToStr()]; !exists {
		s.items[item.ToStr()] = item
		s.Size += 1
	}
}

func (s *Set) Clear() {
	s.items = make(map[string]contracts.T)
	s.Size = 0
}

func (s *Set) Delete(item contracts.T) {
	if _, exists := s.items[item.ToStr()]; exists {
		delete(s.items, item.ToStr())
		s.Size -= 1
	}
}

func (s *Set) ForEach(fn contracts.Callback) {
	for idx, _ := range s.items {
		fn(s.items[idx])
	}
}

func (s *Set) Has(item contracts.T) bool {
	_, exists := s.items[item.ToStr()]
	return exists
}

func (s *Set) Values() []contracts.T {
	v := make([]contracts.T, 0, len(s.items))

	for _, val := range s.items {
		v = append(v, val)
	}

	return v
}

