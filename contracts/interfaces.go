package contracts

type Comparable interface {
	Eq(another Comparable) bool
	LessThan(another Comparable) bool
	GreaterThan(another Comparable) bool
}

type Representable interface {
	ToStr() string
}

type T interface {
	Representable
	Comparable
}
