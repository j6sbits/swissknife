package contracts

type Callback func(elm T)
type Modifier func(elm T) T
type Reducer func(acc T, cur T) T
type Tester func(elm T) bool