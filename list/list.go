package list

import "swissknife/contracts"

type List struct {
	Length int
	items []contracts.T
}

func New() *List {
	return &List{
		Length: 0,
		items: make([]contracts.T, 0),
	}
}

func From(items []contracts.T) *List {
	l := New()
	l.items = items
	l.Length = len(items)

	return l
}

func (l *List) Concat(another *List) {
	l.items = append(l.items, another.items...)
	l.Length += another.Length
}


func (l *List) Every(fn contracts.Tester) bool {
	for _, it := range l.items {
		if !fn(it) {
			return false
		}
	}

	return true
}

func (l *List) Filter(fn contracts.Tester) *List {
	v := New()
	for _, it := range l.items {
		if fn(it) {
			v.Push(it)
		}
	}

	return v
}

func (l *List) Find(item contracts.T) contracts.T {
	for _, it := range l.items {
		if it.Eq(item) {
			return it
		}
	}

	return nil
}

func (l *List) ForEach(fn contracts.Callback) {
	for idx, _ := range l.items {
		fn(l.items[idx])
	}
}

func (l *List) Includes(item contracts.T) bool {
	for idx, _ := range l.items {
		if l.items[idx].Eq(item) {
			return true
		}
	}

	return false
}

func (l *List) Join(prefix string) string {
	rs := ""
	last := l.Length - 1
	for idx, _ := range l.items {
		rs = rs + l.items[idx].ToStr()
		if idx < last {
			rs = rs + ","
		}
	}

	return rs
}

func (l *List) Map(fn contracts.Modifier) *List {
	v := New()
	for _, it := range l.items {
		v.Push(fn(it))
	}

	return v
}

func (l *List) Pop() contracts.T {
	var x contracts.T
	x, l.items = l.items[len(l.items)-1], l.items[:len(l.items)-1]
	l.Length -= 1

	return x
}

func (l *List) Push(item contracts.T) {
	l.items = append(l.items, item)
	l.Length += 1
}

func (l *List) Reduce(fn contracts.Reducer, initValue contracts.T) contracts.T {
	var acc contracts.T
	i := 0

	for _, it := range l.items {
		if i == 0 && initValue != nil {
			acc = initValue
			acc = fn(acc, it)
		} else if i == 0 && initValue == nil {
			acc = it
		} else {
			acc = fn(acc, it)
		}

		i++
	}

	return acc
}

func (l *List) Reverse() {
	for i, j := 0, l.Length - 1; i < j; i, j = i + 1, j - 1 {
		l.items[i], l.items[j] = l.items[j], l.items[i]
	}
}

func (l *List) Slice(start int, length int) *List {
	v := New()
	if start > l.Length {
		return v
	}

	count := 0
	for idx, it := range l.items {
		if start >= idx && count < length {
			v.Push(it)
		}
	}

	return v
}

func (l *List) Sort() *List {
	v := From(l.items)
	var n = v.Length
	for i := 1; i < n; i++ {
		j := i
		for j > 0 {
			if v.items[j-1].GreaterThan(v.items[j]) {
				v.items[j-1], v.items[j] = v.items[j], v.items[j-1]
			}
			j = j - 1
		}
	}

	return v
}
